package com.kshrd.storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kshrd.storage.share_preference.SharePreferenceActivity;
import com.kshrd.storage.util.PreferenceUtil;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // IF Login Success, Execute the code below
                PreferenceUtil.setUserStatus(LoginActivity.this, 3);
                startActivity(new Intent(LoginActivity.this, SharePreferenceActivity.class));
                finish();
            }
        });
    }
}
