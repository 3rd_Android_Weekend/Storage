package com.kshrd.storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.storage.share_preference.SharePreferenceActivity;
import com.kshrd.storage.util.PreferenceUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        int userStatus = PreferenceUtil.getUserStatus(this);

        // TODO: Set Preference
        //PreferenceUtil.setUserStatus(this, [1 or 2 or 3]);

        Intent intent = null;
        switch (userStatus){
            case 1:
                intent = new Intent(this, AppIntroActivity.class);
                break;
            case 2:
                // Start Login
                intent = new Intent(this, LoginActivity.class);
                break;
            case 3:
                // Start Main Screen
                intent= new Intent(this, SharePreferenceActivity.class);
                break;
        }
        startActivity(intent);

        finish();
    }
}
