package com.kshrd.storage.db;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.storage.R;
import com.kshrd.storage.entity.Article;

public class DBActivity extends AppCompatActivity implements RecyclerItemClickListener{

    private EditText etTitle;
    private EditText etDescription;
    private Button btnInsert;
    private RecyclerView rvArticle;
    private ArticleAdapter articleAdapter;
    private ArticleHelper articleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);

        articleHelper = ArticleHelper.getInstance(DBActivity.this);

        initView();
        initEvent();
        setupRecyclerView();
        refreshData();
    }

    private void setupRecyclerView() {
        articleAdapter = new ArticleAdapter();
        articleAdapter.setRecyclerItemClickListener(this);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        rvArticle.setAdapter(articleAdapter);
    }

    private void initEvent() {
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = etTitle.getText().toString();
                String description = etDescription.getText().toString();

                Article article = new Article(title, description);

                if (articleHelper.insert(article)) {
                    Toast.makeText(DBActivity.this, "Insert Successfully", Toast.LENGTH_SHORT).show();
                    refreshData();
                } else {
                    Toast.makeText(DBActivity.this, "Insert Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initView() {
        etTitle = (EditText) findViewById(R.id.etTitle);
        etDescription = (EditText) findViewById(R.id.etDescription);
        btnInsert = (Button) findViewById(R.id.btnInsert);
        rvArticle = (RecyclerView) findViewById(R.id.rvArticle);
    }

    private void refreshData() {
        articleAdapter.clear();
        articleAdapter.addMoreItems(articleHelper.findAll());
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onDeleteClicked(int position) {
        Article article = articleAdapter.findArticle(position);
        if (articleHelper.delete(article.getId())){
            articleAdapter.remove(position);
        } else {
            Toast.makeText(this, "Unable to Delete", Toast.LENGTH_SHORT).show();
        }
    }
}
