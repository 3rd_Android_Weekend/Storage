package com.kshrd.storage.db;

/**
 * Created by pirang on 7/22/17.
 */

public interface RecyclerItemClickListener {

    void onItemClicked(int position);

    void onDeleteClicked(int position);

}
