package com.kshrd.storage.db;

/**
 * Created by pirang on 7/16/17.
 */

public class DBConstant {

    public static final String DB_NAME = "article_db";
    public static final int DB_VERSION = 1;

    public static final String ARTICLE_TABLE = "article";
    public static final String ARTICLE_ID = "id";
    public static final String ARICLE_TITLE = "title";
    public static final String ARICLE_DESCRIPTION = "description";

    public static final String CREATE_ARTICLE_TABLE = "CREATE TABLE " +
            ARTICLE_TABLE + " ( " +
            ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ARICLE_TITLE + " TEXT NOT NULL, " +
            ARICLE_DESCRIPTION + " TEXT NOT NULL )";

}
