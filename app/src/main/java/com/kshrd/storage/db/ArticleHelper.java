package com.kshrd.storage.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kshrd.storage.entity.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 7/16/17.
 */

public class ArticleHelper {

    private DBHelper dbHelper;
    private static ArticleHelper articleHelper;
    private SQLiteDatabase db;

    public static ArticleHelper getInstance(Context context) {
        if (articleHelper == null) {
            articleHelper = new ArticleHelper(context);
        }
        return articleHelper;
    }

    private ArticleHelper(Context context) {
        dbHelper = DBHelper.getInstance(context);
    }

    public boolean insert(Article article) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstant.ARICLE_TITLE, article.getTitle());
        values.put(DBConstant.ARICLE_DESCRIPTION, article.getDescription());
        long result = db.insert(DBConstant.ARTICLE_TABLE, null, values);

        if (result > 0) {
            return true;
        }
        return false;
    }

    public List<Article> findAll() {
        List<Article> articleList = new ArrayList<>();
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(
                DBConstant.ARTICLE_TABLE,
                null,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Article article = Article.getArticleFromCursor(cursor);
                articleList.add(article);
                cursor.moveToNext();
            }
        }
        return articleList;
    }

    public boolean delete(int id) {
        db = dbHelper.getWritableDatabase();
        int result = db.delete(DBConstant.ARTICLE_TABLE, "id = ?", new String[]{String.valueOf(id)});
        if (result > 0) {
            return true;
        }
        return false;
    }

    public boolean update(Article article) {
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBConstant.ARICLE_TITLE, article.getTitle());
        values.put(DBConstant.ARICLE_DESCRIPTION, article.getDescription());
        int result = db.update(DBConstant.ARTICLE_TABLE, values, "id = ?", new String[]{String.valueOf(article.getId())});
        if (result > 0) {
            return true;
        }
        return false;
    }

}

