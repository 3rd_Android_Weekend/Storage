package com.kshrd.storage.db;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.storage.R;
import com.kshrd.storage.entity.Article;

/**
 * Created by pirang on 7/16/17.
 */

public class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

    TextView tvTitle;
    TextView tvDescription;
    ImageView ivDelete;

    private RecyclerItemClickListener recyclerItemClickListener;

    public ArticleViewHolder(View itemView, RecyclerItemClickListener recyclerItemClickListener) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
        ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
        this.recyclerItemClickListener = recyclerItemClickListener;

        ivDelete.setOnClickListener(this);
    }

    public void onBind(Article article){
        tvTitle.setText(article.getTitle());
        tvDescription.setText(article.getDescription());
    }

    @Override
    public void onClick(View view) {
        recyclerItemClickListener.onDeleteClicked(getAdapterPosition());
    }
}
