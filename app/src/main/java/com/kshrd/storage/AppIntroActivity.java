package com.kshrd.storage;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.kshrd.storage.util.PreferenceUtil;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("Title 1", "Description 1", R.mipmap.ic_launcher, ContextCompat.getColor(this, R.color.colorAccent)));
        addSlide(AppIntroFragment.newInstance("Title 2", "Description 2", R.mipmap.ic_launcher, ContextCompat.getColor(this, R.color.colorPrimary)));
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        PreferenceUtil.setUserStatus(this, 2);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
