package com.kshrd.storage.file;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kshrd.storage.R;
import com.kshrd.storage.util.AppConstant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class WriteFileActivity extends AppCompatActivity {

    private static final int WRITE_EXTERNAL_STRORAGE_CODE = 1;
    private static final int READ_EXTERNAL_STRORAGE_CODE = 2;
    private TextView tvDisplay;
    private EditText etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_file);

        etText = (EditText) findViewById(R.id.etText);
        tvDisplay = (TextView) findViewById(R.id.tvDisplay);

        findViewById(R.id.btnWrite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writePrivateData(etText.getText().toString());
            }
        });

        findViewById(R.id.btnRead).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readPrivateData();
            }
        });

        findViewById(R.id.btnWritePublic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WriteFileActivityPermissionsDispatcher.writePublicDataWithCheck(
                        WriteFileActivity.this,
                        etText.getText().toString()
                );
            }
        });

        findViewById(R.id.btnReadPublic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WriteFileActivityPermissionsDispatcher.readPublicDataWithCheck(
                        WriteFileActivity.this
                );
            }
        });
    }

    private void readPrivateData() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            int c;
            FileInputStream fis = openFileInput(AppConstant.TEXT_FILE_NAME);
            while ((c = fis.read()) != -1) {
                char character = (char) c;
                stringBuilder.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            tvDisplay.setText(stringBuilder.toString());
        }
    }

    private void writePrivateData(String str) {

        FileOutputStream fos = null;
        try {
            fos = openFileOutput(AppConstant.TEXT_FILE_NAME, MODE_PRIVATE);
            fos.write(str.getBytes());
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    void readPublicData() {
        StringBuilder stringBuilder = new StringBuilder();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        try {
            int c;
            FileInputStream fis = new FileInputStream(new File(path, AppConstant.TEXT_FILE_NAME));
            while ((c = fis.read()) != -1) {
                char character = (char) c;
                stringBuilder.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            tvDisplay.setText(stringBuilder.toString());
        }
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationalForWriteExternalStorage(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("Please grant Permission to write file!")
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }



    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void writePublicData(String str) {

        // External but Private
        //getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(path, AppConstant.TEXT_FILE_NAME));
            fos.write(str.getBytes());
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForCamera() {
        Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForCamera() {
        Toast.makeText(this, "Never Ask Again", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        WriteFileActivityPermissionsDispatcher.onRequestPermissionsResult(
                WriteFileActivity.this,
                requestCode,
                grantResults
        );
    }
}
