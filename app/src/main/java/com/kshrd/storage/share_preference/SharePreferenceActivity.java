package com.kshrd.storage.share_preference;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kshrd.storage.LoginActivity;
import com.kshrd.storage.R;
import com.kshrd.storage.entity.User;
import com.kshrd.storage.util.AppConstant;
import com.kshrd.storage.util.PreferenceUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharePreferenceActivity extends AppCompatActivity {

    EditText etUserName;
    private List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(AppConstant.USER_PREF, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        etUserName = (EditText) findViewById(R.id.etUserName);

        String userListJson = preferences.getString(AppConstant.USER_LIST, null);

        if (userListJson == null) {
            userList = new ArrayList<>();
        } else {
            Type type = new TypeToken<List<User>>() {
            }.getType();
            userList = new Gson().fromJson(userListJson, type);
        }

        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(1, etUserName.getText().toString());
                userList.add(user);
                // Store Single User Object
                editor.putString(AppConstant.USER_OBJECT, new Gson().toJson(user));
                editor.putString(AppConstant.USER_NAME, etUserName.getText().toString());
                editor.putString(AppConstant.USER_LIST, new Gson().toJson(userList));
                editor.apply();
                showPreferenceData();
            }
        });


    }

    private void showPreferenceData() {
        Intent i = new Intent(this, ShowSharePreferenceActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout){
            PreferenceUtil.setUserStatus(this, 2);
            Intent i = new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
