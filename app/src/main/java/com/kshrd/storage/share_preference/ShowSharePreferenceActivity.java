package com.kshrd.storage.share_preference;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kshrd.storage.R;
import com.kshrd.storage.entity.User;
import com.kshrd.storage.util.AppConstant;

import java.lang.reflect.Type;
import java.util.List;

public class ShowSharePreferenceActivity extends AppCompatActivity {

    TextView tvUserName;
    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_share_preference);

        tvUserName = (TextView) findViewById(R.id.tvUerName);

        SharedPreferences preferences = getSharedPreferences(AppConstant.USER_PREF, MODE_PRIVATE);
        userName = preferences.getString(AppConstant.USER_NAME, "n/a");
        tvUserName.setText(userName);

        String userJson = preferences.getString(AppConstant.USER_OBJECT, "n/a");
        User user = new Gson().fromJson(userJson, User.class);
        tvUserName.setText(user.getName());

        String userListJson = preferences.getString(AppConstant.USER_LIST, null);
        if (userJson != null) {
            Type type = new TypeToken<List<User>>() {
            }.getType();
            List<User> userList = new Gson().fromJson(userListJson, type);
            for (User item : userList){
                Log.e("ooooo", item.getName());
            }
        }


    }
}
