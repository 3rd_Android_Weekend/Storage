package com.kshrd.storage.entity;

import android.database.Cursor;

import com.kshrd.storage.db.DBConstant;

/**
 * Created by pirang on 7/16/17.
 */

public class Article {

    private int id;
    private String title;
    private String description;

    public Article(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Article(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Article getArticleFromCursor(Cursor cursor){
        int id = cursor.getInt(cursor.getColumnIndex(DBConstant.ARTICLE_ID));
        String title = cursor.getString(cursor.getColumnIndex(DBConstant.ARICLE_TITLE));
        String description = cursor.getString(cursor.getColumnIndex(DBConstant.ARICLE_DESCRIPTION));
        return new Article(id, title, description);
    }
}
