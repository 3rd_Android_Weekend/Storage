package com.kshrd.storage.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.kshrd.storage.util.AppConstant;

/**
 * Created by pirang on 7/9/17.
 */

public class PreferenceUtil {

    public static int getUserStatus(Context context) {
        SharedPreferences preferences =
                context.getSharedPreferences(AppConstant.USER_PREF, Context.MODE_PRIVATE);
        int status = preferences.getInt(AppConstant.USER_STATUS, 1);
        return status;
    }

    public static void setUserStatus(Context context, int userStatus) {
        SharedPreferences preferences =
                context.getSharedPreferences(AppConstant.USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(AppConstant.USER_STATUS, userStatus);
        editor.apply();
    }

}
