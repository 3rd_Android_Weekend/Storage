package com.kshrd.storage.util;

/**
 * Created by pirang on 7/9/17.
 */

public class AppConstant {

    public static final String USER_PREF = "user_preference";
    public static final String USER_NAME = "user_name";
    public static final String USER_OBJECT = "user_object";
    public static final String USER_LIST = "user_list";

    public static final String USER_STATUS = "user_status";


    public static final String TEXT_FILE_NAME = "text_file.txt";
}
